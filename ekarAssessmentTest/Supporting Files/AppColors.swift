//
//  AppColors.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import UIKit

struct AppColors {
	
	static let mainBlue: UIColor = {
		return UIColor(named: "mainBlue") ?? rgbaColor(51.0, 198.0, 244.0, 1.0)
	}()
	
	static let secondaryBlue: UIColor = {
		return UIColor(named: "secondaryBlue") ?? rgbaColor(116.0, 198.0, 245.0, 1.0)
	}()
	
	static let tertiaryBlue: UIColor = {
		return UIColor(named: "tertiaryBlue") ?? rgbaColor(109.0, 222.0, 251.0, 1.0)
	}()
	
	static let backgroundBlue: UIColor = {
		return UIColor(named: "backgroundBlue") ?? rgbaColor(226.0, 246.0, 254.0, 1.0)
	}()
	
	static let mainRed: UIColor = {
		return UIColor(named: "mainRed") ?? rgbaColor(236.0, 81.0, 113.0, 1.0)
	}()
	
	static let mainBlack: UIColor = {
		return UIColor(named: "mainBlack") ?? rgbaColor(47.0, 70.0, 75.0, 1.0)
	}()
	
	static func rgbaColor(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat) -> UIColor {
		return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
	}
	
}
