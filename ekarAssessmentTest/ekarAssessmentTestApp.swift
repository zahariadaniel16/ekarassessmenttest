//
//  ekarAssessmentTestApp.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 26.10.2021.
//

import SwiftUI

@main
struct ekarAssessmentTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
				.environmentObject(LocationManager.shared)
				.environmentObject(LoadingViewManager.shared)
        }
    }
}
