//
//  LocationManager.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import UIKit
import Foundation
import CoreLocation
import MapKit

class LocationManager: NSObject, ObservableObject {
	
	static let shared: LocationManager = LocationManager()
	
	@Published var region = MKCoordinateRegion(
		center: CLLocationCoordinate2D(
			latitude: 25.09547498919254,
			longitude: 55.17289851297365),
		span: MKCoordinateSpan(
			latitudeDelta: 0.025,
			longitudeDelta: 0.025))
	
	private var isLocationPermissionEnabled: Bool = false
	private var shouldUpdateRegion: Bool = true
	
	private let locationManager = CLLocationManager()
	
	override init() {
		super.init()
		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		
		if [CLAuthorizationStatus.authorizedAlways,
			CLAuthorizationStatus.authorizedWhenInUse]
			.contains(locationManager.authorizationStatus) {
			isLocationPermissionEnabled = true
		} else {
			isLocationPermissionEnabled = false
		}
	}
	
	public func requestLocationAuthorization() {
		locationManager.requestWhenInUseAuthorization()
	}
	
	public func startUpdatingLocation() {
		guard isLocationPermissionEnabled else { return }
		locationManager.startUpdatingLocation()
	}
	
	public func getCurrentLocation() {
		guard isLocationPermissionEnabled else { return }
		shouldUpdateRegion = true
	}
	
	private func randomNumbersBetween(_ first: CGFloat, _ second: CGFloat) -> CGFloat {
		return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(first - second) + min(first, second)
	}
	
	public func generate100RandomCoordinates() -> [CLLocationCoordinate2D] {
		var coordinates: [CLLocationCoordinate2D] = []
		var index = 0
		while index != 100 {
			index += 1
			let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(randomNumbersBetween(-90, 90)),
													longitude: CLLocationDegrees(randomNumbersBetween(-180, 180)))
			coordinates.append(coordinate)
		}
		return coordinates
	}
}


extension LocationManager: CLLocationManagerDelegate {
	
	func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
		switch manager.authorizationStatus {
		case .authorizedWhenInUse, .authorizedAlways:
			isLocationPermissionEnabled = true
			startUpdatingLocation()
		case .denied, .restricted, .notDetermined:
			isLocationPermissionEnabled = false
		default:
			isLocationPermissionEnabled = false
			break
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		guard let location = locations.first else { return }
		DispatchQueue.main.async {
			if self.shouldUpdateRegion {
				self.shouldUpdateRegion = false
				self.region = MKCoordinateRegion(
					center: location.coordinate,
					span: MKCoordinateSpan(latitudeDelta: 0.025,
										   longitudeDelta: 0.025))
			}
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		debugPrint("Failes to get current location. Error: \(error.localizedDescription)")
	}
}
