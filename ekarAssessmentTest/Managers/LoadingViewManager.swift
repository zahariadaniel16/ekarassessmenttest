//
//  LoadingViewManager.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import UIKit
import SwiftUI

class LoadingViewManager: NSObject, ObservableObject {

	@Published var isLoading = false
	
	static let shared: LoadingViewManager = LoadingViewManager()
	
}
