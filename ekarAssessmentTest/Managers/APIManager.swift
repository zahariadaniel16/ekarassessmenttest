//
//  APIManager.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import Foundation
import CoreLocation

enum VehicleDetailsError: Error {
	case invalidInputs
	case invalidVin
	case noData
	case apiNotEnabled
	case urlError
	case unknownError
}

class APIManager: NSObject {
	
	static let shared: APIManager = APIManager()
	
	private let apiKey: String = "tha91z6lv_j8u1nv4xs_ilfswb1e3"
	
	private let baseURL: String = "https://api.carsxe.com/"
	
	enum APIRequestPathType: String {
		case specs
		case images
	}
	
	static let newJSONDecoder: JSONDecoder = {
		let decoder = JSONDecoder()
		if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
			decoder.dateDecodingStrategy = .iso8601
		}
		return decoder
	}()
	
	static let newJSONEncoder: JSONEncoder = {
		let encoder = JSONEncoder()
		if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
			encoder.dateEncodingStrategy = .iso8601
		}
		return encoder
	}()
	
	
	// MARK: - Private Methods
	
	private func getRequestURLString(for requestPathType: APIRequestPathType, options: [String : String]) -> String? {
		var urlString: String = baseURL
		urlString += requestPathType.rawValue
		
		var queryItems: [String] = []
		
		queryItems.append("key=\(apiKey)")
		for (key, value) in options {
			let queryItem: String = "\(key)=\(value)"
			queryItems.append(queryItem)
		}
		let queryString: String = queryItems.joined(separator: "&")
		
		urlString += "?\(queryString)"
		
		return urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
	}
	
	private func executeRequest(for request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
		URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
			DispatchQueue.main.async {
				completionHandler(data, response, error)
			}
		}).resume()
	}
	
	
	// MARK: - Public Methods
	
	public func getVehiclesMarkers(completion: @escaping ([VehicleMarker]) -> Void) {
		DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
			let coordinates: [CLLocationCoordinate2D] = LocationManager.shared.generate100RandomCoordinates()
			var vehiclesMarkers: [VehicleMarker] = []
			for coordinate in coordinates {
				let vehicleMarker = VehicleMarker(coordinate: coordinate,
												  vin: VehicleModel.defaultVINList.randomElement() ?? "JTDZN3EU0E3298500")
				vehiclesMarkers.append(vehicleMarker)
			}
			completion(vehiclesMarkers)
		}
	}
	
	public func getVehicleDetails(options: [String : String], completion: @escaping (VehicleModel?, Error?) -> Void) {
		guard let urlString: String = getRequestURLString(for: .specs, options: options),
			  let url = URL(string: urlString) else {
			completion(nil, VehicleDetailsError.urlError)
			return
		}
		
		executeRequest(for: URLRequest(url: url)) { data, response, error in
			if let data = data, let vehicleModel = try? VehicleModel(data: data) {
				completion(vehicleModel, nil)
			} else {
				completion(nil, VehicleDetailsError.unknownError)
			}
		}
	}
	
	public func getVehicleImages(options: [String : String], completion: @escaping ([VehicleImageModel]?, Error?) -> Void) {
		guard let urlString: String = getRequestURLString(for: .images, options: options),
			  let url = URL(string: urlString) else {
			completion(nil, VehicleDetailsError.urlError)
			return
		}
		
		executeRequest(for: URLRequest(url: url)) { data, response, error in
			if let data = data, let vehicleImagesSearchModel = try? VehicleImagesSearchModel(data: data) {
				if vehicleImagesSearchModel.success {
					completion(vehicleImagesSearchModel.images, nil)
				} else {
					completion(nil, VehicleDetailsError.noData)
				}
			}
		}
	}
	
	
}
