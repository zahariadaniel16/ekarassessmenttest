//
//  ToastView.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 10.11.2021.
//

import SwiftUI

struct ToastView: View {
	
	@Binding var isPresented: Bool
	@Binding var icon: String
	@Binding var message: String
	
	var body: some View {
		Group {
			if isPresented {
				VStack(spacing: 16) {
					Image(systemName: icon)
						.resizable()
						.frame(width: 32, height: 32)
						.foregroundColor(.white)
					
					Text(message)
						.bold()
						.foregroundColor(.white)
						.multilineTextAlignment(.center)
				}
				.padding(32)
				.background(
					RoundedRectangle(cornerRadius: 10)
						.foregroundColor(.accentColor.opacity(0.9))
						.shadow(color: .black.opacity(0.2), radius: 10, x: 0, y: 0))
			} else {
				EmptyView()
			}
		}
	}
}

struct ToastView_Previews: PreviewProvider {
    static var previews: some View {
		ToastView(isPresented: .constant(true), icon: .constant("checkmark.circle.fill"), message: .constant("Thank you for choosing ekar"))
    }
}
