//
//  CaptureImageCoordinator.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 09.11.2021.
//

import SwiftUI

class CaptureImageCoordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
	
	@Binding var isCoordinatorPresented: Bool
	@Binding var coordinatorImage: Image?
	
	init(isPresented: Binding<Bool>, image: Binding<Image?>) {
		_isCoordinatorPresented = isPresented
		_coordinatorImage = image
	}
	
	func imagePickerController(_ picker: UIImagePickerController,
							   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
		
		coordinatorImage = Image(uiImage: image)
		isCoordinatorPresented = false
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		isCoordinatorPresented = false
	}
}
