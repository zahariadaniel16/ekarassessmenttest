//
//  OnBoardView.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 26.10.2021.
//

import SwiftUI

struct OnBoardView: View {
	
	struct VehiclePhotoItem: Identifiable {
		var id: Int
		var type: String
		var image: Image
		var isUploaded: Bool
	}
	
	@Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
	
	@State var vehiclePhotosItems: [VehiclePhotoItem] = [
		VehiclePhotoItem(id: 0,
						 type: "FRONT/LEFT",
						 image: Image(systemName: "car.fill"),
						 isUploaded: false),
		VehiclePhotoItem(id: 1,
						 type: "FRONT/RIGHT",
						 image: Image(systemName: "car.fill"),
						 isUploaded: false),
		VehiclePhotoItem(id: 2,
						 type: "BACK/LEFT",
						 image: Image(systemName: "car.fill"),
						 isUploaded: false),
		VehiclePhotoItem(id: 3,
						 type: "BACK/LEFT",
						 image: Image(systemName: "car.fill"),
						 isUploaded: false)
	]
	
	let vehiclePhotosItemsColumns = [GridItem(.adaptive(minimum: 150))]
	
	@State private var presentCaptureImageView: Bool = false
	@State private var selectedVehiclePhotoItemId: Int = -1
	@State private var capturedImage: Image?
	
	@State private var comment: String = ""
	
	@State private var presentAlert: Bool = false
	@State private var alertMessage: String = ""
	
	@State private var presentToast: Bool = false
	@State private var toastIcon: String = "checkmark.circle.fill"
	@State private var toastMessage: String = "Thank you for choosing ekar"
	
    var body: some View {
		VStack {
			Text("Please upload clear photos of the vehicle to avoid liability of any damages caused before starting your reservation")
				.foregroundColor(.init(AppColors.mainBlack))
				.padding(.horizontal, 26)
				.padding(.bottom, 32)
			
			LazyVGrid(columns: vehiclePhotosItemsColumns, spacing: 0) {
				ForEach(vehiclePhotosItems, id: \.id) { item in
					Button {
						selectedVehiclePhotoItemId = item.id
						presentCaptureImageView.toggle()
					} label: {
						VStack {
							item.image
								.resizable()
								.scaledToFit()
								.frame(width: 100, height: 100)
								.foregroundColor(.init(AppColors.mainBlack))
							Text(item.type)
								.font(.subheadline)
								.foregroundColor(.init(AppColors.mainBlack))
						}
					}
					.padding()
				}
			}
			.overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray.opacity(0.2), lineWidth: 2))
			.padding(.horizontal, 26)
			
			Spacer()
			
			VStack {
				HStack {
					Text("Leave a comment:")
						.bold()
						.font(.subheadline)
						.foregroundColor(.init(AppColors.mainBlack))
					Spacer()
				}
				
				TextField("Enter comment", text: $comment)
					.autocapitalization(.sentences)
					.font(.subheadline)
					.foregroundColor(.init(AppColors.mainBlack))
				Divider()
			}
			.padding(.horizontal, 26)
			
			Button(action: {
				if isDataValid() {
					LoadingViewManager.shared.isLoading = true
					DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
						LoadingViewManager.shared.isLoading = false
						self.presentToast = true
						DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
							self.presentToast = false
							self.presentationMode.wrappedValue.dismiss()
						}
					}
				}
			}, label: {
				Text("Submit")
					.foregroundColor(.white)
					.bold()
					.frame(maxWidth: .infinity, maxHeight: 60)
					.background(RoundedRectangle(cornerRadius: 10)
									.foregroundColor(.init(AppColors.mainBlue)))
			})
				.padding(.horizontal, 26)
		}
		.padding(.vertical, 16)
		.toolbar {
			ToolbarItem(placement: ToolbarItemPlacement.principal) {
				Text("ekar")
					.foregroundColor(.accentColor)
					.font(.title)
					.bold()
			}
		}
		.alert(isPresented: $presentAlert) {
			Alert(
				title: Text("Attention"),
				message: Text(alertMessage),
				dismissButton: .default(Text("OK"), action: {
					presentAlert = false
				})
			)
		}
		.fullScreenCover(isPresented: $presentCaptureImageView) {
			if let capturedImage = capturedImage {
				vehiclePhotosItems[selectedVehiclePhotoItemId].image = capturedImage
				vehiclePhotosItems[selectedVehiclePhotoItemId].isUploaded = true
				resetCaptureImageView()
			}
		} content: {
			CaptureImageView(isPresented: $presentCaptureImageView, image: $capturedImage)
				.ignoresSafeArea()
		}
		.overlay {
			ToastView(isPresented: $presentToast, icon: $toastIcon, message: $toastMessage)
		}
    }
	
	private func resetCaptureImageView() {
		capturedImage = nil
		presentCaptureImageView = false
		selectedVehiclePhotoItemId = -1
	}
	
	private func isDataValid() -> Bool {
		for vehiclePhotoItem in vehiclePhotosItems {
			if !vehiclePhotoItem.isUploaded {
				alertMessage = "All four vehicle photos are mandatory! Please capture the missing photos."
				presentAlert.toggle()
				return false
			}
		}
		if comment.isEmpty {
			alertMessage = "Comment field is mandatory! Please enter a comment."
			presentAlert.toggle()
			return false
		}
		return true
	}
}

struct OnBoardView_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardView()
    }
}
