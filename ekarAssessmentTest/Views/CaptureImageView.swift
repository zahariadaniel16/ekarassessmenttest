//
//  CaptureImageView.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 09.11.2021.
//

import SwiftUI

struct CaptureImageView {
	
	@Binding var isPresented: Bool
	@Binding var image: Image?
	
	func makeCoordinator() -> CaptureImageCoordinator {
		return CaptureImageCoordinator(isPresented: $isPresented, image: $image)
	}
}

extension CaptureImageView: UIViewControllerRepresentable {
	func makeUIViewController(context: UIViewControllerRepresentableContext<CaptureImageView>) -> UIImagePickerController {
		let picker = UIImagePickerController()
		picker.delegate = context.coordinator
		picker.sourceType = .camera
		return picker
	}
	
	func updateUIViewController(_ uiViewController: UIImagePickerController,
								context: UIViewControllerRepresentableContext<CaptureImageView>) {
	}
}
