//
//  LoadingView.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import SwiftUI

struct LoadingView: View {
	
	@Binding var isAnimating: Bool
	
    var body: some View {
		Group {
			if isAnimating {
				ZStack {
					Color.black
						.frame(maxWidth: .infinity, maxHeight: .infinity)
						.opacity(0.3)
						.onTapGesture {
							// here would have been a dismiss action
						}
					Color.white
						.frame(width: 70, height: 70)
						.cornerRadius(14)
						.shadow(color: .black.opacity(0.2), radius: 5, x: 0, y: 5)
					ProgressView()
						.progressViewStyle(CircularProgressViewStyle(tint: .accentColor))
				}
			} else {
				EmptyView()
			}
		}
		.ignoresSafeArea()
    }
}

struct LoadingView_Previews: PreviewProvider {
	static var previews: some View {
		LoadingView(isAnimating: .constant(true))
	}
}
