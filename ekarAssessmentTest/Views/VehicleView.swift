//
//  VehicleView.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 26.10.2021.
//

import SwiftUI

struct VehicleView: View {
	
	@Binding var vehicle: VehicleModel
	@Binding var vehicleImagesURLsDictionary: [String : String]
	
	@State private var contractLength: String = "1"
	@State private var sliderValue: Float = 1
	
	@State private var presentAlert: Bool = false
	@State private var presentOnBoardView: Bool = false
	
	let keyFeatures: [String] = ["Keyless Entry",
								 "Bluetooth",
								 "AM / FM",
								 "Power Windows",
								 "ABS Brakes with EBD",
								 "AUX / USB Jack"]
	
	let keyFeaturesColumns = [GridItem(.adaptive(minimum: 120), spacing: 8, alignment: .leading)]
	
    var body: some View {
		VStack {
			ScrollView(showsIndicators: false) {
				VStack(spacing: 16) {
					TabView {
						ForEach([VehicleImageAngle.front, VehicleImageAngle.side, VehicleImageAngle.back], id: \.self) { angle in
							AsyncImage(url: getVehicleImageURL(angle: angle)) { image in
								image
									.resizable()
									.scaledToFit()
									.padding(32)
							} placeholder: {
								ProgressView()
							}
						}
					}
					.tabViewStyle(PageTabViewStyle())
					.indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
					.frame(height: 250)
					.background(Color.white)
					
					HStack {
						Text("Year - \(vehicle.attributes.year)")
							.bold()
							.font(.caption2)
							.foregroundColor(.init(AppColors.mainBlack))
						Spacer()
						HStack {
							Text("Available colors")
								.bold()
								.font(.caption2)
							.foregroundColor(.init(AppColors.mainBlack))
							Color.black
								.frame(width: 8, height: 8)
								.clipShape(Circle())
							Color.white
								.frame(width: 8, height: 8)
								.clipShape(Circle())
							Color.gray
								.frame(width: 8, height: 8)
								.clipShape(Circle())
						}
					}
					.padding(.horizontal, 26)
					.padding(.bottom, 5)
					
					VStack(spacing: 8) {
						HStack {
							Text("Base Price")
								.bold()
								.font(.subheadline)
								.foregroundColor(.init(AppColors.mainBlack))
							Spacer()
							Text("Contract Length")
								.bold()
								.font(.subheadline)
								.foregroundColor(.init(AppColors.mainBlack))
						}
						
						HStack {
							HStack(alignment: .lastTextBaseline) {
								Text("1,500")
									.bold()
									.font(.largeTitle)
									.foregroundColor(.init(AppColors.mainBlack))
								Text("AED / MONTH")
									.bold()
									.font(.subheadline)
									.foregroundColor(.init(AppColors.mainBlack))
							}
							Spacer()
							HStack(alignment: .lastTextBaseline) {
								Text(contractLength)
									.bold()
									.font(.largeTitle)
									.foregroundColor(.init(AppColors.mainBlack))
								Text("Months")
									.bold()
									.font(.subheadline)
									.foregroundColor(.init(AppColors.mainBlack))
							}
						}
					}
					.padding(.horizontal, 26)
					.padding(.bottom, 10)
					
					VStack {
						HStack {
							Text("TENURE")
								.font(.caption2)
								.foregroundColor(.init(AppColors.mainBlack))
							Spacer()
						}
						
						HStack {
							Text("1 to 9 Months")
								.bold()
								.font(.headline)
								.foregroundColor(.init(AppColors.mainBlack))
							Spacer()
							Text("SAVINGS OF AED 1,500")
								.bold()
								.font(.caption2)
								.foregroundColor(.white)
								.padding(.horizontal, 8)
								.padding(.vertical, 5)
								.background(Capsule()
												.foregroundColor(.init(AppColors.mainRed)))
						}
						
						Slider(value: $sliderValue, in: 0.0...10.0, step: 1.0) {
							Text("Contract Length")
						} onEditingChanged: { editing in
							if !editing {
								if sliderValue < 1 {
									sliderValue = 1
									contractLength = "1"
								} else if sliderValue < 2 {
									sliderValue = 1
									contractLength = "1"
								} else if sliderValue < 4.5 {
									sliderValue = 3
									contractLength = "3"
								} else if sliderValue < 7.5 {
									sliderValue = 6
									contractLength = "6"
								} else {
									sliderValue = 9
									contractLength = "9"
								}
							}
						}
					}
					.padding(.horizontal, 26)
					.padding(.bottom, 10)
					
					VStack {
						HStack {
							Text("BOOKING FEE")
								.bold()
								.font(.caption2)
								.foregroundColor(.init(AppColors.mainBlack))
							Spacer()
						}
						
						HStack {
							HStack(alignment: .lastTextBaseline) {
								Text("AED")
									.bold()
									.font(.subheadline)
									.foregroundColor(.init(AppColors.mainBlack))
								Text("120")
									.bold()
									.font(.largeTitle)
									.foregroundColor(.init(AppColors.mainBlack))
							}
							Spacer()
							
							Button {
								presentAlert = true
							} label: {
								Text("how contracts work?")
									.bold()
									.font(.headline)
									.padding(.horizontal, 16)
									.padding(.vertical, 8)
									.background(RoundedRectangle(cornerRadius: 10)
													.foregroundColor(.white)
									)
									.overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.init(AppColors.mainBlue), lineWidth: 2))
								
							}
							.alert(isPresented: $presentAlert) {
								Alert(
									title: Text("How contracts work?"),
									message: Text("Please read Terms & Conditions"),
									dismissButton: .default(Text("OK"), action: {
										presentAlert = false
									})
								)
							}
						}
					}
					.padding(.horizontal, 26)
					.padding(.bottom, 16)
					
					VStack {
						HStack {
							Text("About the vehicle")
								.bold()
								.font(.title3)
								.foregroundColor(.init(AppColors.mainBlack))
							Spacer()
						}
						.padding(.horizontal, 26)
						
						HStack(spacing: 16) {
							VStack(spacing: 8) {
								Image(systemName: "car.fill")
								Text("\(getVehicleEngine()) Engine")
									.bold()
									.font(.caption2)
									.foregroundColor(.init(AppColors.mainBlack))
							}
							.frame(width: getVehicleDetailsItemSize(), height: getVehicleDetailsItemSize())
							.background(Color.init(AppColors.backgroundBlue))
							.clipShape(RoundedRectangle(cornerRadius: 10))
							
							VStack(spacing: 8) {
								Image(systemName: "figure.wave")
								Text(getVehicleDoors())
									.bold()
									.font(.caption2)
									.foregroundColor(.init(AppColors.mainBlack))
							}
							.frame(width: getVehicleDetailsItemSize(), height: getVehicleDetailsItemSize())
							.background(Color.init(AppColors.backgroundBlue))
							.clipShape(RoundedRectangle(cornerRadius: 10))
							
							VStack(spacing: 8) {
								Image(systemName: "gearshape.2.fill")
								Text(getVehicleTransmission())
									.bold()
									.font(.caption2)
									.foregroundColor(.init(AppColors.mainBlack))
							}
							.frame(width: getVehicleDetailsItemSize(), height: getVehicleDetailsItemSize())
							.background(Color.init(AppColors.backgroundBlue))
							.clipShape(RoundedRectangle(cornerRadius: 10))
							
							VStack(spacing: 8) {
								Image(systemName: "fuelpump.fill")
								Text(getVehicleFuelType())
									.bold()
									.font(.caption2)
									.foregroundColor(.init(AppColors.mainBlack))
							}
							.frame(width: getVehicleDetailsItemSize(), height: getVehicleDetailsItemSize())
							.background(Color.init(AppColors.backgroundBlue))
							.clipShape(RoundedRectangle(cornerRadius: 10))
						}
						
						HStack {
							Text("Key Features")
								.bold()
								.font(.caption)
								.foregroundColor(.init(AppColors.mainBlack))
							Spacer()
						}
						.padding(.horizontal, 26)
						.padding(.top, 10)
						
						LazyVGrid(columns: keyFeaturesColumns, spacing: 12) {
							ForEach(keyFeatures, id: \.self) { item in
								Text(item)
									.bold()
									.font(.caption2)
									.foregroundColor(.init(AppColors.mainBlack))
									.padding(.horizontal, 16)
									.padding(.vertical, 5)
									.background(Color.init(AppColors.backgroundBlue))
									.clipShape(Capsule())
							}
						}
						.padding(.horizontal, 26)
						
					}
					.padding(.top, 24)
					.background(Color.white)
				}
				.frame(maxWidth: .infinity)
				.background(Color.init(AppColors.backgroundBlue))
			}
			
			VStack(spacing: 16) {
				HStack(spacing: 8) {
					VStack(alignment: .leading, spacing: 8) {
						HStack {
							Text(vehicle.attributes.make)
								.bold()
								.font(.title3)
								.foregroundColor(.init(AppColors.mainBlack))
							Text(vehicle.attributes.model)
								.font(.title3)
								.foregroundColor(.init(AppColors.mainBlack))
						}
						Text(getVehicleStyle())
							.bold()
							.font(.footnote)
							.foregroundColor(.init(AppColors.mainBlack))
					}
					
					Spacer()
				}
				
				Button(action: {
					presentOnBoardView = true
				}, label: {
					Text("Proceed with your selection")
						.foregroundColor(.white)
						.bold()
						.frame(maxWidth: .infinity, maxHeight: 60)
						.background(RoundedRectangle(cornerRadius: 8)
										.foregroundColor(.init(AppColors.secondaryBlue)))
				})
			}
			.padding(.vertical, 40)
			.padding(.horizontal, 56)
			.background(Color.init(UIColor.white))
		}
		.background(
			NavigationLink("OnBoardView", isActive: $presentOnBoardView) {
				OnBoardView()
			}
		)
		.toolbar {
			ToolbarItem(placement: ToolbarItemPlacement.principal) {
				Text("ekar")
					.foregroundColor(.accentColor)
					.font(.title)
					.bold()
			}
		}
		.onAppear {
			presentOnBoardView = false
		}
    }
	
	private func getVehicleImageURL(angle: VehicleImageAngle) -> URL? {
		guard !vehicleImagesURLsDictionary.isEmpty else { return nil }
		if let string = vehicleImagesURLsDictionary[angle.rawValue] {
			return URL(string: string)
		}
		return nil
	}
	
	private func getVehicleDetailsItemSize() -> CGFloat {
		let screenWidth: CGFloat = UIScreen.main.bounds.size.width
		let padding: CGFloat = 26
		let spacing: CGFloat = 16
		let numberOfItems: CGFloat = 4
		return (screenWidth - 2 * padding - 3 * spacing) / numberOfItems
	}
	
	private func getVehicleEngine() -> String {
		return vehicle.attributes.engine.components(separatedBy: " ").first ?? ""
	}
	
	private func getVehicleDoors() -> String {
		return vehicle.attributes.style.components(separatedBy: " ").last ?? ""
	}
	
	private func getVehicleTransmission() -> String {
		return vehicle.attributes.transmission.components(separatedBy: " ").last ?? ""
	}
	
	private func getVehicleFuelType() -> String {
		if let fuelType = vehicle.attributes.engine.components(separatedBy: " ").last {
			switch fuelType {
			case "DIESEL", "PETROL", "HYBRID":
				return fuelType.capitalized
			default:
				return "?"
			}
		}
		return "?"
	}
	
	private func getVehicleStyle() -> String {
		return vehicle.attributes.style.components(separatedBy: " ").first ?? ""
	}
}

struct VehicleView_Previews: PreviewProvider {
    static var previews: some View {
		VehicleView(vehicle: .constant(VehicleModel.default),
					vehicleImagesURLsDictionary: .constant([
						"front":
			"https://st.motortrend.com/uploads/sites/10/2015/11/2013-toyota-prius-v-wagon-five-hatchback-front-view.png",
						
			"side": "https://st.motortrend.com/uploads/sites/10/2015/11/2015-toyota-prius-v-wagon-four-hatchback-side-view.png",
			"back":
						"https://st.motortrend.com/uploads/sites/10/2015/11/2012-toyota-prius-v-wagon-five-hatchback-rear-view.png"]))
    }
}
