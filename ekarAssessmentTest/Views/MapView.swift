//
//  MapView.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 26.10.2021.
//

import SwiftUI
import MapKit

struct MapView: View {
	
	@EnvironmentObject var locationManager: LocationManager
	
	@State private var vehicleMarkers: [VehicleMarker] = []
	
	@State private var isVehicleLoaded: Bool = false
	@State private var vehicle: VehicleModel = VehicleModel.default
	@State private var vehicleImagesURLsDictionary: [String : String] = [
		"front":
		"https://st.motortrend.com/uploads/sites/10/2015/11/2013-toyota-prius-v-wagon-five-hatchback-front-view.png",
					
		"side": "https://st.motortrend.com/uploads/sites/10/2015/11/2015-toyota-prius-v-wagon-four-hatchback-side-view.png",
		"back":
					"https://st.motortrend.com/uploads/sites/10/2015/11/2012-toyota-prius-v-wagon-five-hatchback-rear-view.png"]
	
    var body: some View {
		NavigationView {
			ZStack(alignment: .bottom) {
				Map(coordinateRegion: $locationManager.region, showsUserLocation: true, annotationItems: vehicleMarkers) { vehicleMarker in
					MapAnnotation(coordinate: vehicleMarker.coordinate, anchorPoint: CGPoint(x: 0.5, y: 0.5)) {
						ZStack {
							DropPin()
								.frame(width: 40, height: 60, alignment: .center)
								.foregroundColor(.white)
								.shadow(radius: 10, x: 2, y: 2)
							Image(systemName: "car.circle.fill")
								.resizable()
								.frame(width: 30, height: 30, alignment: .center)
								.foregroundColor(.accentColor)
						}
						.onTapGesture {
							isVehicleLoaded = false
							getVehicleDetails(vin: vehicleMarker.vin)
						}
					}
				}
				.edgesIgnoringSafeArea(.bottom)
				
				HStack {
					Spacer()
					Button {
						locationManager.getCurrentLocation()
					} label: {
						ZStack {
							Circle()
								.frame(width: 44, height: 44)
								.foregroundColor(.accentColor)
							Image(systemName: "location.fill")
								.resizable()
								.frame(width: 22, height: 22)
								.foregroundColor(.white)
						}
					}
				}
				.padding(.trailing, 16)
				.padding(.bottom, 32)
			}
			.background(
				NavigationLink("Vehicle Details", isActive: $isVehicleLoaded, destination: {
					VehicleView(vehicle: $vehicle, vehicleImagesURLsDictionary: $vehicleImagesURLsDictionary)
				})
			)
			.navigationBarTitleDisplayMode(.inline)
			.navigationViewStyle(StackNavigationViewStyle())
			.toolbar {
				ToolbarItem(placement: ToolbarItemPlacement.principal) {
					Text("ekar")
						.foregroundColor(.accentColor)
						.font(.title)
						.bold()
				}
			}
			.toolbar {
				ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
					Button {
						getVehicleMarkers()
					} label: {
						Image(systemName: "arrow.counterclockwise.circle.fill")
							.resizable()
							.frame(width: 32, height: 32)
							.foregroundColor(.accentColor)
					}

				}
			}
		}
		.onAppear {
			locationManager.requestLocationAuthorization()
			locationManager.startUpdatingLocation()
			getVehicleMarkers()
		}
		.onChange(of: vehicleImagesURLsDictionary) { newValue in
			// is all 3 image URLs received from API (front, side, back)
			// then we can present the vehicle view
			if newValue.count == 3 {
				self.isVehicleLoaded = true
			}
		}
    }
	
	private func getVehicleMarkers() {
		LoadingViewManager.shared.isLoading = true
		DispatchQueue.global(qos: .userInitiated).async {
			APIManager.shared.getVehiclesMarkers { vehicleMarkers in
				DispatchQueue.main.async {
					LoadingViewManager.shared.isLoading = false
					self.vehicleMarkers = vehicleMarkers
				}
			}
		}
	}
	
	private func getVehicleDetails(vin: String) {
		let options: [String : String] = [
			"vin": vin
		]
		LoadingViewManager.shared.isLoading = true
		DispatchQueue.global(qos: .userInitiated).async {
			APIManager.shared.getVehicleDetails(options: options) { vehicle, error in
				DispatchQueue.main.async {
					LoadingViewManager.shared.isLoading = false
					if let vehicle = vehicle {
						self.vehicle = vehicle
						self.getVehicleImages()
					}
				}
			}
		}
	}
	
	private func getVehicleImages() {
		self.vehicleImagesURLsDictionary = [:]
		getVehicleImage(angle: .front)
		getVehicleImage(angle: .side)
		getVehicleImage(angle: .back)
	}
	
	private func getVehicleImage(angle: VehicleImageAngle) {
		let options: [String : String] = [
			"make": vehicle.attributes.make,
			"model": vehicle.attributes.model,
			"angle": angle.rawValue,
			"transparent": "true",
		]
		LoadingViewManager.shared.isLoading = true
		DispatchQueue.global(qos: .userInitiated).async {
			APIManager.shared.getVehicleImages(options: options) { images, error in
				DispatchQueue.main.async {
					LoadingViewManager.shared.isLoading = false
					if let image = images?.first {
						self.vehicleImagesURLsDictionary[angle.rawValue] = image.thumbnailLink
					}
				}
			}
		}
	}
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
