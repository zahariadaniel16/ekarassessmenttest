//
//  ContentView.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 26.10.2021.
//

import SwiftUI

struct ContentView: View {
	
	@EnvironmentObject var loadingViewManager: LoadingViewManager
	
    var body: some View {
        MapView()
			.overlay(LoadingView(isAnimating: $loadingViewManager.isLoading))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
