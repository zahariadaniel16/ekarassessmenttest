//
//  VehicleModel.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import Foundation

enum Equipment: String, Codable {
	case nA = "N/A"
	case opt = "Opt."
	case std = "Std."
}

struct VehicleModel: Codable {
	let attributes: VehicleAttributesModel
	let colors: [VehicleColorModel]
	let equipment: [String: Equipment]
	let warranties: [Warranty]
	
	init(data: Data) throws {
		self = try APIManager.newJSONDecoder.decode(VehicleModel.self, from: data)
	}

	init(_ json: String, using encoding: String.Encoding = .utf8) throws {
		guard let data = json.data(using: encoding) else {
			throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
		}
		try self.init(data: data)
	}
	
	static let defaultVINList: [String] = ["JTDZN3EU0E3298500",
										   "1FT8X3BT0BEA61538",
										   "WDDSJ4EB7EN128078",
										   "5TDYK3DC8DS290235",
										   "4T1BF22K5WU057633"]
	
	static let `default`: VehicleModel = try! VehicleModel("{\"attributes\":{\"year\":\"2014\",\"make\":\"Toyota\",\"model\":\"PriusV\",\"trim\":\"Three\",\"style\":\"HATCHBACK4-DR\",\"type\":\"\",\"size\":\"\",\"category\":\"\",\"made_in\":\"JAPAN\",\"made_in_city\":\"\",\"doors\":\"\",\"fuel_type\":\"\",\"fuel_capacity\":\"11.90gallon\",\"city_mileage\":\"44miles/gallon\",\"highway_mileage\":\"40miles/gallon\",\"engine\":\"1.8LL4DOHC16VHYBRID\",\"engine_size\":\"\",\"engine_cylinders\":\"\",\"transmission\":\"ContinuouslyVariableTransmission\",\"transmission_short\":\"CVT\",\"transmission_type\":\"\",\"transmission_speeds\":\"\",\"drivetrain\":\"FWD\",\"anti_brake_system\":\"4-WheelABS\",\"steering_type\":\"R&P\",\"curb_weight\":\"3274lbs\",\"gross_vehicle_weight_rating\":\"4321lbs\",\"overall_height\":\"62.00in.\",\"overall_length\":\"181.70in.\",\"overall_width\":\"69.90in.\",\"wheelbase_length\":\"109.40in.\",\"standard_seating\":\"5\",\"invoice_price\":\"$25,613USD\",\"delivery_charges\":\"$810USD\",\"manufacturer_suggested_retail_price\":\"$27,515USD\",\"production_seq_number\":\"298500\",\"front_brake_type\":\"Disc\",\"rear_brake_type\":\"Drum\",\"turning_diameter\":\"36.10in.\",\"front_suspension\":\"Ind\",\"rear_suspension\":\"Semi\",\"front_spring_type\":\"Coil\",\"rear_spring_type\":\"Coil\",\"tires\":\"205/60R16\",\"front_headroom\":\"39.60in.\",\"rear_headroom\":\"38.60in.\",\"front_legroom\":\"41.30in.\",\"rear_legroom\":\"35.90in.\",\"front_shoulder_room\":\"55.90in.\",\"rear_shoulder_room\":\"55.20in.\",\"front_hip_room\":\"53.50in.\",\"rear_hip_room\":\"53.50in.\",\"interior_trim\":[\"Bisque\",\"DarkGray\",\"MistyGray\"],\"exterior_color\":[\"BarcelonaRedMet\",\"Black\",\"BlizzardPearl\",\"BlueRibbonMetallic\",\"ClassicSilverMetallic\",\"ClearSkyMetallic\",\"MagneticGrayMetallic\",\"SeaGlassPearl\"],\"curb_weight_manual\":\"\",\"ground_clearance\":\"5.70in.\",\"track_front\":\"60.60in.\",\"track_rear\":\"60.80in.\",\"cargo_length\":\"\",\"width_at_wheelwell\":\"\",\"width_at_wall\":\"\",\"depth\":\"\",\"optional_seating\":\"\",\"passenger_volume\":\"97.20cu.ft.\",\"cargo_volume\":\"34.30cu.ft.\",\"standard_towing\":\"\",\"maximum_towing\":\"\",\"standard_payload\":\"915lbs\",\"maximum_payload\":\"915lbs\",\"maximum_gvwr\":\"4321lbs\"},\"colors\":[{\"category\":\"Interior\",\"name\":\"Bisque\"},{\"category\":\"Interior\",\"name\":\"DarkGray\"},{\"category\":\"Interior\",\"name\":\"MistyGray\"},{\"category\":\"Exterior\",\"name\":\"BarcelonaRedMet\"},{\"category\":\"Exterior\",\"name\":\"Black\"},{\"category\":\"Exterior\",\"name\":\"BlizzardPearl\"},{\"category\":\"Exterior\",\"name\":\"BlueRibbonMetallic\"},{\"category\":\"Exterior\",\"name\":\"ClassicSilverMetallic\"},{\"category\":\"Exterior\",\"name\":\"ClearSkyMetallic\"},{\"category\":\"Exterior\",\"name\":\"MagneticGrayMetallic\"},{\"category\":\"Exterior\",\"name\":\"SeaGlassPearl\"}],\"equipment\":{\"4wd_awd\":\"N/A\",\"abs_brakes\":\"Std.\",\"adjustable_foot_pedals\":\"N/A\",\"air_conditioning\":\"Std.\",\"alloy_wheels\":\"Std.\",\"am_fm_radio\":\"N/A\",\"automatic_headlights\":\"Std.\",\"automatic_load_leveling\":\"N/A\",\"cargo_area_cover\":\"Std.\",\"cargo_area_tiedowns\":\"Std.\",\"cargo_net\":\"Opt.\",\"cassette_player\":\"N/A\",\"cd_changer\":\"N/A\",\"cd_player\":\"Std.\",\"child_safety_door_locks\":\"Std.\",\"chrome_wheels\":\"N/A\",\"cruise_control\":\"Std.\",\"daytime_running_lights\":\"Std.\",\"deep_tinted_glass\":\"N/A\",\"driver_airbag\":\"Std.\",\"driver_multi_adjustable_power_seat\":\"N/A\",\"dvd_player\":\"N/A\",\"electrochromic_exterior_rearview_mirror\":\"N/A\",\"electrochromic_interior_rearview_mirror\":\"N/A\",\"electronic_brake_assistance\":\"Std.\",\"electronic_parking_aid\":\"N/A\",\"first_aid_kit\":\"Opt.\",\"fog_lights\":\"N/A\",\"front_air_dam\":\"N/A\",\"front_cooled_seat\":\"N/A\",\"front_heated_seat\":\"N/A\",\"front_power_lumbar_support\":\"Std.\",\"front_power_memory_seat\":\"N/A\",\"front_side_airbag\":\"Std.\",\"front_side_airbag_with_head_protection\":\"N/A\",\"front_split_bench_seat\":\"N/A\",\"full_size_spare_tire\":\"N/A\",\"genuine_wood_trim\":\"N/A\",\"glass_rear_window_on_convertible\":\"N/A\",\"heated_exterior_mirror\":\"N/A\",\"heated_steering_wheel\":\"N/A\",\"high_intensity_discharge_headlights\":\"N/A\",\"interval_wipers\":\"N/A\",\"keyless_entry\":\"Std.\",\"leather_seat\":\"N/A\",\"leather_steering_wheel\":\"N/A\",\"limited_slip_differential\":\"N/A\",\"load_bearing_exterior_rack\":\"N/A\",\"locking_differential\":\"N/A\",\"locking_pickup_truck_tailgate\":\"Std.\",\"manual_sunroof\":\"Opt.\",\"navigation_aid\":\"Std.\",\"passenger_airbag\":\"Std.\",\"passenger_multi_adjustable_power_seat\":\"N/A\",\"pickup_truck_bed_liner\":\"N/A\",\"pickup_truck_cargo_box_light\":\"N/A\",\"power_adjustable_exterior_mirror\":\"N/A\",\"power_door_locks\":\"Std.\",\"power_sliding_side_van_door\":\"N/A\",\"power_sunroof\":\"Opt.\",\"power_trunk_lid\":\"N/A\",\"power_windows\":\"Std.\",\"rain_sensing_wipers\":\"N/A\",\"rear_spoiler\":\"Std.\",\"rear_window_defogger\":\"Std.\",\"rear_wiper\":\"Std.\",\"remote_ignition\":\"Opt.\",\"removable_top\":\"N/A\",\"run_flat_tires\":\"N/A\",\"running_boards\":\"N/A\",\"second_row_folding_seat\":\"Std.\",\"second_row_heated_seat\":\"N/A\",\"second_row_multi_adjustable_power_seat\":\"N/A\",\"second_row_removable_seat\":\"N/A\",\"second_row_side_airbag\":\"N/A\",\"second_row_side_airbag_with_head_protection\":\"N/A\",\"second_row_sound_controls\":\"N/A\",\"separate_driver_front_passenger_climate_controls\":\"N/A\",\"side_head_curtain_airbag\":\"Std.\",\"skid_plate\":\"N/A\",\"sliding_rear_pickup_truck_window\":\"N/A\",\"splash_guards\":\"Opt.\",\"steel_wheels\":\"N/A\",\"steering_wheel_mounted_controls\":\"Std.\",\"subwoofer\":\"N/A\",\"tachometer\":\"N/A\",\"telematics_system\":\"N/A\",\"telescopic_steering_column\":\"Std.\",\"third_row_removable_seat\":\"N/A\",\"tilt_steering\":\"Std.\",\"tilt_steering_column\":\"Std.\",\"tire_pressure_monitor\":\"Std.\",\"tow_hitch_receiver\":\"N/A\",\"towing_preparation_package\":\"N/A\",\"traction_control\":\"Std.\",\"trip_computer\":\"Std.\",\"trunk_anti_trap_device\":\"N/A\",\"vehicle_anti_theft\":\"Std.\",\"vehicle_stability_control_system\":\"Std.\",\"voice_activated_telephone\":\"N/A\",\"wind_deflector_for_convertibles\":\"N/A\"},\"warranties\":[{\"type\":\"Basic\",\"miles\":\"36,000mile\",\"months\":\"36month\"},{\"type\":\"Powertrain\",\"miles\":\"60,000mile\",\"months\":\"60month\"},{\"type\":\"Rust\",\"months\":\"60month\",\"miles\":\"Unlimitedmile\"}]}")
	
}
