//
//  VehicleDetailsSearchModel.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import Foundation

struct VehicleDetailsSearchInputModel: Codable {
	let key, vin: String
}

struct VehicleDetailsSearchModel: Codable {
	let success: Bool
	let input: VehicleDetailsSearchInputModel
	let vehicle: VehicleModel
	let timestamp: String
}
