//
//  VehicleColorModel.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import Foundation

enum Category: String, Codable {
	case exterior = "Exterior"
	case interior = "Interior"
}

struct VehicleColorModel: Codable {
	let category: Category
	let name: String
}
