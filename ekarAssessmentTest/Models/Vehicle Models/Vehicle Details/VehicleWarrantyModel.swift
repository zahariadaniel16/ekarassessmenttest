//
//  VehicleWarrantyModel.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import Foundation

struct Warranty: Codable {
	let type, miles, months: String
}
