//
//  VehicleImagesSearchModel.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import Foundation

enum VehicleImageAngle: String {
	case front
	case side
	case back
}

struct VehicleImagesSearchModel: Codable {
//	let query: Query
	let images: [VehicleImageModel]
	let success: Bool
	let error: String
	
	init(data: Data) throws {
		self = try APIManager.newJSONDecoder.decode(VehicleImagesSearchModel.self, from: data)
	}

	init(_ json: String, using encoding: String.Encoding = .utf8) throws {
		guard let data = json.data(using: encoding) else {
			throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
		}
		try self.init(data: data)
	}
}
