//
//  VehicleImageModel.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 01.11.2021.
//

import Foundation

enum MIME: String, Codable {
	case imageJPEG = "image/jpeg"
	case imagePNG = "image/png"
}

struct VehicleImageModel: Codable {
	let mime: MIME
	let link: String
	let contextLink: String
	let height, width, byteSize: Int
	let thumbnailLink: String
	let thumbnailHeight, thumbnailWidth: Int
	let hostPageDomainFriendlyName, accentColor, datePublished: String
}
