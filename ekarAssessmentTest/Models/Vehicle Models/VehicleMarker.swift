//
//  VehicleMarker.swift
//  ekarAssessmentTest
//
//  Created by Zaharia Daniel on 09.11.2021.
//

import UIKit
import CoreLocation

struct VehicleMarker: Identifiable {
	let id = UUID()
	var coordinate: CLLocationCoordinate2D
	let vin: String
}
